---
title:  "Simulation de compagnie de taxi"
author: Laurent Pierron (Laurent.Pierron@inria.fr)
affiliation: INRIA / Université de Lorraine
tags: [nothing, nothingness]
documentclass: scrartcl
date: 20 Janvier 2020
papersize: a4paper
geometry: "left=15mm, top=15mm, bottom=10mm, right=15mm, footskip=2mm"
output:
  pdf_document: default
  word_document:
    pandoc_args:
     '--lua-filter=page-break.lua'
...
