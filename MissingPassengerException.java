
/**
 * Signale qu'il n'y avait pas de passager au lieu de prise en charge.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2011.07.31
 */
public class MissingPassengerException extends RuntimeException
{
    private Vehicule vehicule;
    /**
     * Constructeur pour les objets de la classe MissingPassengerException.
     * @param vehicle Le véhicule espérant un passager.
     */
    public MissingPassengerException(Vehicule vehicule)
    {
        super("Passager manquant au lieu de prise en charge.");
    }

    /**
     * @return Le véhicule pour lequel il n'y avait pas de passager.
     */
    public Vehicule getVehicule()
    {
        return vehicule;
    }
}
